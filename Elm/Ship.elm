import Color
import Debug
import Graphics.Collage exposing (..)
import Graphics.Element exposing (..)
import Keyboard
import List exposing (..)
import Random
import Text
import Time
import Window

-- Controls: arrows + space. You can miss up to 3 asteroids. Collision with asteroid
-- or with game boundary kills you. Gook luck!

type alias Positioned a = { a | x:Float, y:Float }
type alias Sized a = { a | w:Float, h:Float }
type alias Ship = Positioned (Sized {})
type alias Bullet = Positioned {}
type alias Asteroid = Positioned (Sized {})
type alias Direction = Positioned {}
type alias Velocity = Positioned {}

type alias State = { ship : Ship
                   , bullets : List Bullet
                   , asteroids : List Asteroid
                   , direction : Direction
                   , lives : Int
                   , seed : Random.Seed
                   , end : Bool
                   }
                
type Event = Tick Float | Fire | Move Direction

gameSize         = { w=500, h=500 }
shipVelocity     = 500 -- px per second
bulletVelocity   = 1000 -- px per second
initialLives     = 3
asteroidVelocity = 150
asteroidSize     = { w=50, h=50 }
initialShip      = { x=-200, y=0, w=70, h=20 }
initialState     = { ship      = initialShip
                   , direction = {x=0, y=0}
                   , end       = False
                   , lives     = initialLives
                   , bullets   = []
                   , asteroids = []
                   , seed      = Random.initialSeed 37426
                   }

neg x = 0 - x

add : Positioned a -> Positioned b -> Positioned a
add a b = {a | x <- a.x + b.x, y <- a.y + b.y}

mul c v = { v | x <- c * v.x, y <- c * v.y }

isOutsideScreen : Positioned a -> Bool
isOutsideScreen {x,y} =
  let left   = neg (gameSize.w / 2)
      right  = neg left
      bottom = neg (gameSize.h / 2)
      top    = neg bottom
  in  x < left || x > right || y < bottom || y > top

pointInside rect point =
  let left   = rect.x - rect.w / 2
      right  = left + rect.w
      bottom = rect.y - rect.h / 2
      top    = bottom + rect.h
  in  point.x >= left && point.x <= right && point.y >= bottom && point.y <= top

rectCollides rect rect' = 
  let left   = rect.x - rect.w / 2
      right  = left + rect.w
      bottom = rect.y - rect.h / 2
      top    = bottom + rect.h
  in any (pointInside rect') [ {x=left,y=top}, {x=left,y=bottom}, {x=right,y=top}, {x=right,y=bottom} ]

clock = Time.fps 60
input = Signal.mergeMany
          [ Signal.map (Time.inSeconds >> Tick) clock
          , Signal.map (\{x,y} -> Move {x=toFloat x, y=toFloat y}) Keyboard.arrows
          , Signal.map (\_ -> Fire) Keyboard.space
          ]

shipStep : Float -> State -> State
shipStep dt s = { s | ship <- add s.ship (mul (dt * shipVelocity) s.direction) }

bulletsStep : Float -> State -> State
bulletsStep dt s =
  let moveBullet b = { b | x <- b.x + bulletVelocity * dt }
  in  {s | bullets <- s.bullets |> map moveBullet |> filter (not << isOutsideScreen) }

asteroidsStep dt s =
  let moveAsteroid a = { a | x <- a.x - asteroidVelocity * dt }
      thisStays a    = (not (isOutsideScreen a)) && (all (not << pointInside a) s.bullets)
      (stay, leave)  = s.asteroids |> map moveAsteroid |> partition thisStays
      lives          = s.lives - if isEmpty (filter isOutsideScreen leave) then 0 else 1
  in  {s | asteroids <- s.asteroids |> map moveAsteroid |> filter thisStays, lives <- lives }

collisionStep s =
  if any (rectCollides s.ship) s.asteroids then { s | end <- True } else s

step : State -> Float -> State
step s dt = if | isOutsideScreen s.ship -> { s | end <- True }
               | s.lives <= 0 -> { s | end <- True }
               | otherwise    -> s |> shipStep dt |> bulletsStep dt |> tryAddAsteroid |> asteroidsStep dt |> collisionStep

makeAsteroid y = { w = asteroidSize.w, h = asteroidSize.h, x = gameSize.w / 2 - 10, y = y }

tryAddAsteroid s =
  let (prob, seed1) = Random.generate (Random.float 0 1) s.seed
      (posy, seed2) = Random.generate (Random.float (neg (gameSize.h / 2)) (gameSize.h / 2)) seed1
  in if | prob > 0.04 -> { s | seed <- seed1 }
        | otherwise   -> { s | seed <- seed2, asteroids <- makeAsteroid posy :: s.asteroids }

addBullet : State -> State
addBullet s =
  let bullet = { x=s.ship.x + s.ship.w/2, y=s.ship.y }
  in  { s | bullets <- bullet :: s.bullets }

update : Event -> State -> State
update e s = case e of
    Tick dt  -> step s dt
    Fire     -> addBullet s
    Move d   -> { s | direction <- d }

ifNotEnd f e s = (if s.end then s else f e s)

gameState = Signal.foldp (ifNotEnd update) initialState input

drawBullet {x,y}       = move (x, y) (filled Color.green (rect 2 2))
drawAsteroid {x,y,w,h} = move (x, y) (filled Color.green (rect w h))

drawGame s = collage (round gameSize.w) (round gameSize.h) (
    [ move (s.ship.x, s.ship.y) (filled Color.red (rect s.ship.w s.ship.h))
    , outlined (solid Color.black) (rect gameSize.w gameSize.h)
    ] `append`
    map drawBullet s.bullets `append`
    map drawAsteroid s.asteroids `append`
    [move (neg (gameSize.w/2) + 20, neg (gameSize.h/2) + 20) (toForm (show s.lives))]
    )

drawGameOver = show "Game over"

draw s = if | s.end     -> drawGameOver
            | otherwise -> drawGame s

main = Signal.map draw gameState
