\documentclass[10pt]{beamer}
\usepackage[utf8]{inputenc}
\usepackage{polski}
\usepackage{graphicx}
\usepackage{syntax}

\title{Towards internet of code}
\author{Łukasz Dąbek}
\date{May 27, 2015}

\newcommand{\vs}{\\ \vspace{0.5cm}}

\begin{document}
\maketitle


% PART 1: bashing the lens library

\begin{frame}
    \frametitle{This was supposed to be about \texttt{lens} library!}
    Yes, but...
\end{frame}

\begin{frame}
    \frametitle{This was supposed to be about \texttt{lens} library!}
    \begin{center}\includegraphics[height=10cm]{lens-deps.png}\end{center}
\end{frame}


% PART 2: Motivation: The Haskell <-> JS sharing problem

\begin{frame}
    \frametitle{The internet}
    We already have internet od data\pause, but we do not have internet of code.
\end{frame}

\begin{frame}[fragile]
    \frametitle{The problem}
    Suppose we are writing a website in Haskell.
    Example: customized gift store.
    \pause \vs
    % Why Address? Different countries, different taxes.
    \texttt{price :: Product -> [Addons] -> Address -> Money}
    \pause \vs
    \texttt{function price(product, addons, address) \{ ... \}}
\end{frame}

\begin{frame}
    \frametitle{The problem}
    Client needs to know total cost of order. It is not just a sum of prices!
    \begin{itemize}
        \item Many shipping options $\implies$ different prices.
        \item Shipping discount for big orders.
        \item \emph{Buy two Combulbulators and get third FOR FREE!}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Non-solutions}
    \begin{itemize}
        \item Make a request to server after each state change \pause (kills performance). \pause
        \item Implement logic in Haskell and JavaScript \pause (maintenance nightmare). \pause
        \item Write in JavaScript on the servers \pause (don't even get me started on this). \pause
        \item Embedding Haskell interpreter in JavaScript \pause (cumbersome). \pause
        \item Use language compiled to native code and JavaScript (think \texttt{js_of_ocaml}) \pause
            (not a bad solution really!).
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Core of the problem}
    We have \emph{shared logic}, operating on \emph{shared data structures}.
    \vs
    In most use cases the functions implementing shared logic are pure.
\end{frame}

\begin{frame}
    \frametitle{The problem \#2}
    We are writing social media client for Android and iOS. User interface
    code is completely separate, but code for data fetching should be
    almost the same.
    \pause \vs
    Sometimes we are interested in sharing little more than pure functions.
\end{frame}

% We are starting to get the rabbit out of the hat.

\begin{frame}
    \frametitle{The idea}
    Small language as a target for compilation \emph{and decompilation}.
    Think of high level assembly language.
    \pause \vs
    It should be:
    \begin{itemize}
        \item pure,
        \item functional,
        \item simple, but expressive,
        \item typed, maybe even dependently typed.
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Integration}
    From practical point of view interaction with shared language should
    be hassle free. This is wrong:
    \vs
    \begin{verbatim}
var ctx = new Morte.Context();
ctx.loadFile(...);
ctx.callFunction("price", Morte.ADT.List(...), ...);
    \end{verbatim}
\end{frame}

\begin{frame}[fragile]
    This is better:
    \begin{verbatim}
import '/my/awesome/library/prices';
price([product1], [], shipping_address);
    \end{verbatim}
    \pause
    After importing code it should be indistinguishable from JavaScript code in use.
\end{frame}

\begin{frame}
    In practice the system will do more interesting things, like mapping
    data types to representation idiomatic in host language. We will talk
    about this at the end.
\end{frame}


% PART 3: STLC -> F -> Fw -> CoC

\begin{frame}[fragile]
    \frametitle{The language}
    We will construct desired language. Let's start with simply typed lambda calculus:
    \begin{align*}
        E &= x\ |\ \lambda (x:T). E\ |\ E\ E \\
        T &= X\ |\ T \rightarrow T
    \end{align*}
    \pause \vs
    I will use another syntax:
    \texttt{(fun (x:A) => x) y}
    \pause \vs
    What can we express in this language?
\end{frame}

\begin{frame}[fragile]
    \frametitle{Booleans}
    \texttt{true = fun (x:A) (b:A) => x}
    \vs
    \texttt{false = fun (x:A) (b:A) => y}
    \pause \vs
    \texttt{if b x y = b x y}
\end{frame}

\begin{frame}
    \frametitle{Natural numbers}
    \texttt{zero = fun (f:A -> A) (z:A) => z}
    \vs
    \texttt{one = fun (f:A -> A) (z:A) => f z}
    \pause \vs
    \texttt{succ n = fun (f:A -> A) (z:A) => f (n f z)}
    \pause \vs
    \texttt{add n m = fun (f:A -> A) (z:A) => n f (m f z)}
    \vs
    \texttt{mul n m = fun (f:A -> A) (z:A) => n (m f) z}
\end{frame}

\begin{frame}
    Does it look like a \texttt{fold}?
\end{frame}

\begin{frame}
    \frametitle{STLC -- problem}
    How to express identity function?
    \vs
    \texttt{fun (x:A) => x} is not polymorphic! We need richer type system.
\end{frame}

\begin{frame}
    \frametitle{System F}
    Polymorphic lambda calculus. We can quantify over types:
    \vs
    \texttt{id = fun (A:*) (x:A) => x} - polymorphic identity.
    \pause \vs
    The type of identity function is:\\
    \texttt{id : forall (A:*). A -> A}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Pairs}
    \texttt{Pair A B = forall (R:*). (A -> B -> R) -> R}
    \vs
    \begin{verbatim}
fst : forall (A B:*).
      (forall (R:*). (A -> B -> R) -> R) -> A
\end{verbatim}
    In pseudonotation: \texttt{fst: forall (A B:*). Pair A B -> A}.
    \pause \vs
    \texttt{fst A B p = p A (fun (x:A) (y:B) => x)}\\
    \texttt{snd A B p = p B (fun (x:A) (y:B) => y)}
\end{frame}

\begin{frame}
    \frametitle{Natural number, honestly}
    \texttt{Nat = forall (R:*). R -> (R -> R) -> R}
    \vs
    Implementation of common functions are same as in STLC.
\end{frame}

\begin{frame}[fragile]
    \frametitle{Lists}
    \texttt{List A = forall (R:*). R -> (A -> R -> R) -> R}
    \vs
    \texttt{nil : forall (A:*). List A}\\
    \texttt{cons : forall (A:*). A -> List A -> List A}\\
    \texttt{map : forall(A B:*). (A -> B) -> List A -> List B}
    \pause \vs
    \begin{verbatim}
map A B f xs = xs (List B) (nil B xs) 
    (fun (x:A) (ys:List B) => cons B (f x) ys)
    \end{verbatim}
\end{frame}

\begin{frame}
    We can represent algebraic data types in System F. \pause What else can we do?
\end{frame}

\begin{frame}[fragile]
    \frametitle{Existential types}
    Suppose that we have a module with hidden type \texttt{S} and functions \texttt{f : S -> S},
    \texttt{g : S -> Nat} and constant \texttt{c : S}. How to express it in System F?
    \pause \vs
    \begin{verbatim}
forall (R:*).
  (forall (S:*). S -> (S -> S) -> (S -> Nat) -> R) -> R
    \end{verbatim}
    \pause
\end{frame}

\begin{frame}
    \frametitle{System F$\omega$}
    To get rid of pseudonotation for polymorphic list we need another, richer type system called F$\omega$.
    \vs
    In a nutshell: we are introducing higher kinded types, also known
    as type constructors.
\end{frame}

\begin{frame}[fragile]
    \frametitle{Lists, honestly}
    \texttt{List : * -> *}\\
    \begin{verbatim}
List = fun (A:*) => forall (R:* -> *).
         R A -> (A -> R A -> R A) -> R A
\end{verbatim}
    Nothing else changed much.
\end{frame}

\begin{frame}
    \frametitle{Calculus of Constructions}
    Dependently typed version of F$\omega$, basis for Coq (Calculus of Inductive Constructions).
\end{frame}


% PART 4 - Strong normalization

\begin{frame}
    \frametitle{Strong normalization}
    All of the mentioned languages are strongly normalizing.
    \vs
    What about the Android/iOS problem?
\end{frame}

\begin{frame}
    \frametitle{Possibly infinite behaviors}
    We can use streams for that! That was one of the first versions of Haskell I/O.
    \pause \vs
    And because of strong normalization property we have progress guarantee for free.
\end{frame}


% PART 5 - Free monads

\begin{frame}
    If we have time left, we shall take a look at free monads.
\end{frame}


% PART 6 - Practical problems and conclusion

\begin{frame}
    \frametitle{Implementation -- Morte and Annah}
    Morte is core language -- currently something between CoC and System F$\omega$.
    \pause \vs
    Annah is higher level language compiled to \emph{and from} Morte. Imports over network works now.
    \vs
    Most interesting feature -- translating Morte data definitions into inductive definitions, GADT style.
    \pause \vs
    Work in progress -- decompilation of Annah to Haskell.
\end{frame}

\begin{frame}
    \frametitle{Implementation -- Morte and Annah}
    The author is Garbriel Gonzalez, author of ,,Haskell for all'' blog.
    \vs
    You can check out his Github profile and dive into the code!
\end{frame}

\begin{frame}
    Other solutions? LLVM? asm.js? One language to rule them all?
\end{frame}

\begin{frame}
    Thank you. Any questions?
\end{frame}

\end{document}
