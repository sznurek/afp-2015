module Solutions where

{- Author: Łukasz Dąbek, solutions for exercises for
   Applicative Functors talks by Sebastian Cielemęcki
-}

-- Exercise 1

data MappedList a = MappedList Bool [a] deriving (Show, Eq)
instance Functor MappedList where
    fmap f (MappedList _ a) = MappedList True (map f a)

-- This instance breaks law fmap id = id:

isMappedListValid = fmap id a == a where
    a = MappedList False [1,2,3]

-- > isMappedListValid
-- False


-- Exercise 2

data Pair b a = Pair b a
instance Functor (Pair b) where
    fmap f (Pair a b) = Pair a (f b)

{-
instance Applicative (Pair b) where
    -- First problem: how to get something of type b?
    pure a = (_, a)

    -- Second problem: how to combine b1 and b2?
    (Pair b1 f) <*> (Pair b2 a) = Pair _ (f a)
-}

-- If there was and Applicative instance for Pair b I could derive False:
-- let (Pair false 0) = pure 0 :: Pair Void Int


-- Exercise 3

newtype ZipListBad a = ZipListBad [a] deriving (Eq, Show)

instance Functor ZipListBad where
    fmap f (ZipListBad xs) = ZipListBad (map f xs)

instance Applicative ZipListBad where
    pure a = ZipListBad [a]
    (ZipListBad fs) <*> (ZipListBad xs) = ZipListBad (zipWith ($) fs xs)

-- Law `pure id <*> v == v` does not hold:

isZipListValid = (pure id <*> ZipListBad [1,2,3]) == ZipListBad [1,2,3]

-- > isZipListValid
-- False

-- Now the valid instance:
newtype ZipList a = ZipList { unZipList :: [a] } deriving (Eq, Show)

instance Functor ZipList where
    fmap f (ZipList xs) = ZipList (map f xs)

instance Applicative ZipList where
    pure a = ZipList (repeat a)
    (ZipList fs) <*> (ZipList xs) = ZipList (zipWith ($) fs xs)


-- Exercise 4

transp' :: [[a]] -> [[a]]
transp' [] = unZipList (pure [])
transp' (xs:xss) = unZipList $ pure (:) <*> ZipList xs <*> ZipList (transp' xss)


-- Exercise 5

instance Monad ZipList where
    return = ZipList . repeat
    (ZipList []) >>= _ = ZipList []
    (ZipList (x:xs)) >>= f = ZipList (x' : xs') where
        x' = head $ unZipList $ f x
        xs' = unZipList ( (ZipList xs) >>= zlTail . f )
        zlTail (ZipList xs) = ZipList (tail xs)

-- There is a monad law, that `return a >>= f == f a`, but...

isZipListValidMonad = (return 1 >>= \x -> ZipList [x, x]) == ZipList [1, 1]

-- > isZipListValidMonad
-- False


-- Exercise 6

-- a)
fmap' :: Monad m => (a -> b) -> (m a -> m b)
fmap' f m = m >>= return . f

pure' :: Monad m => a -> m a
pure' = return

ap' :: Monad m => m (a -> b) -> m a -> m b
ap' f a = do
    fun <- f
    arg <- a
    return (fun arg)

-- b)

data MyEither a b = Left' a | Right' b

instance Monad (MyEither e) where
    return = Right'
    Left' e  >>= _ = Left' e
    Right' a >>= f = f a

{-
 - Equational reasoning:
 -
 - pure' a = return a = Right' a
 -
 - app mf ma = mf >>= \f -> ma >>= \a -> return (f a) =
 - case mf of
 -   Left' e  -> Left' e
 -   Right' f -> case ma of
 -                Left' e  -> Left' e
 -                Right' a -> Right' (f a)
 -}

instance Applicative (MyEither e) where
    pure = Right'
    (Left' e) <*> _ = Left' e
    _ <*> (Left' e) = Left' e
    (Right' f) <*> (Right' a) = Right' (f a)

instance Functor (MyEither e) where
    fmap _ (Left' e)  = Left' e
    fmap f (Right' a) = Right' (f a)

