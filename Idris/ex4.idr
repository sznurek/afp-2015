
%default total

data C = R | B

data Tree : Type where
  Leaf : Tree
  Node : Tree -> Tree -> Tree

data Path : Tree -> Type where
  End   : Path Leaf
  StepL : Path l -> Path (Node l r)
  StepR : Path r -> Path (Node l r)

insert : (t:Tree) -> Path t -> Tree -> Tree
insert Leaf _ t = t
insert (Node l r) (StepL pl) t = Node (insert l pl t)  r
insert (Node l r) (StepR pr) t = Node l (insert r pr t)

