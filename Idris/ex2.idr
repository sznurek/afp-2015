import Prelude.Functor

%default total

data Fin : Nat -> Type where
  FZ : Fin (S n)
  FS : Fin n -> Fin (S n)

fin2nat : Fin n -> Nat
fin2nat FZ = 0
fin2nat (FS f) = S (fin2nat f)

nat2fin : Nat -> (n : Nat) -> Maybe (Fin n)
nat2fin _ Z         = Nothing
nat2fin Z (S _)     = Just FZ
nat2fin (S m) (S n) = map FS (nat2fin m n)

discZS : Nat -> Type
discZS Z = ()
discZS (S _) = Void

fin2natInjective : (m : Fin k) -> (n : Fin k) -> (fin2nat m = fin2nat n) -> m = n
fin2natInjective FZ FZ eq           = Refl
fin2natInjective FZ (FS o) eq       = void (replace {P = discZS} eq ())
fin2natInjective (FS o) FZ eq       = void (replace {P = discZS} (sym eq) ())
fin2natInjective (FS m') (FS n') eq = cong (fin2natInjective m' n' (succInjective _ _ eq))

