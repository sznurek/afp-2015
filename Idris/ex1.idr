Pair' : Type -> Type -> Type
Pair' a b = (r:Type) -> (a -> b -> r) -> r

MkPair' : a -> b -> Pair' a b
MkPair' a b r f = f a b
